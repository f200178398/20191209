﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SA;
public class UIManager : MonoBehaviour
{
    [Header("Controller UI")]
    public FightSystem fightSystem;
    public Image hpBar;
    public Image hpBarBackGround;
    public GameObject controller;
    public float c_CurrentHp;

    [Header("Boss UI")]
    public AI_data bossData;
    public GameObject boss;
    public Canvas bossHpCanvas;    
    public Image BossHpBar;
    public float BossCurrentHp;
    public float fillamount;
    float lerpSpeed = 0.2f;
    float lerpspeedSlow = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
        hpBar = GameObject.Find("C_CurrentHP").GetComponent<Image>();
        hpBarBackGround = GameObject.Find("C_Hp_BG").GetComponent<Image>();
        controller = GameObject.Find("Controller");

        bossHpCanvas = GameObject.Find("BossHpBar").gameObject.GetComponent<Canvas>();      
        boss = GameObject.Find("Boss");
        bossData = boss.gameObject.GetComponent<AI_data>();
        BossHpBar = GameObject.Find("BossCurrentHP").GetComponent<Image>();
        bossHpCanvas.gameObject.SetActive(false);
        if (!gameObject.activeInHierarchy  && gameObject.tag == ("Enemy"))
        {
            print(gameObject.name);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(ControllerHp());
        StartCoroutine(BossHp());
    }
    IEnumerator ControllerHp()
    {
        c_CurrentHp = fightSystem.currentHp;
        float chp = fightSystem.GetHealthRate();
        hpBar.fillAmount = Mathf.Lerp(hpBar.fillAmount, chp, lerpSpeed);

        yield return new WaitForSeconds(3f);
        hpBarBackGround.fillAmount = Mathf.Lerp(hpBar.fillAmount, chp, lerpspeedSlow);

        
    }
    
    IEnumerator BossHp()
    {
        float hp = bossData.GetHealthRate();
        BossHpBar.fillAmount = Mathf.Lerp(BossHpBar.fillAmount, hp, lerpSpeed);
        float dis =  Vector3.Distance(controller.transform.position , boss.transform.position);
        if( dis < 30f)
        {
            bossHpCanvas.gameObject.SetActive(true);
        }
        if( hp<= 0)
        {
            yield return new WaitForSeconds(3);
            bossHpCanvas.gameObject.SetActive(false);
        }
    }

}
