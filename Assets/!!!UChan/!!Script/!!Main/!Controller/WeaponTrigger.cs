﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class WeaponTrigger : MonoBehaviour
    {
        //****用 來控制武器的Collider開關後與敵人的觸發


        public bool attackTri;
        public FightSystem fightSy;
        public Collider col;
        bool attack;
        public float tempTime;
        public float cd = 1f;
        private void Awake()
        {
            fightSy = GameObject.Find("FightSystem"). GetComponent<FightSystem>();
        }
        private void Start()
        {
            
        }
        private void Update()
        {
            
        }
        

        public void OnTriggerEnter(Collider target) //判定武器是否接觸到敵人
        {
            //attackTri = true;
            
            if (target.gameObject.tag == "Enemy"  /*&& StateManager.Instance().rt*/)
            {
                              
                attackTri = true;
                
            }
            else
            {
                attackTri = false;
                
            }

        }
        public void OnTriggerExit(Collider target) // 
        {
            if (target.gameObject.tag == "Enemy")  // 如果目標不是敵人, 則不會觸發武器判定
            {
                //attackObj = null;
                attackTri = false;

            }
        }
    }

}
