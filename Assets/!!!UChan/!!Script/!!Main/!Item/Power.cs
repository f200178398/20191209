﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class Power : MonoBehaviour
{

    public float power;
    public bool getPower;
    
   
   
    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == ("Controller"))
        {
            getPower = true;
            Destroy(gameObject , 0.1f);
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == ("Controller"))
        {
            getPower = false;
        }
    }

}
