﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble_1 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        BubbleLife();
        BubbleMove();
    }

    [Header("Bubble Life Settings")]
    public float bubbleLife=10.0f;
    void BubbleLife()
    {
        bubbleLife -= Time.deltaTime;//子彈的生命，持續時間
        if (bubbleLife < 0) { Destroy(gameObject); }//當時間<0時，子彈消滅
    }

    [Header("Bubble Transform Settings")]
    public float bubbleSpeed=4.0f;
    void BubbleMove()
    {
        this.transform.position += this.transform.forward * /*這邊控制子彈的速度*/bubbleSpeed * Time.deltaTime;
    }

    public void OnTriggerEnter(Collider other)//這邊寫子彈撞到的東西會怎樣
    {//這邊只寫，泡泡撞到泡泡不會毀滅，泡泡不會互相影響
        if (other.gameObject.layer != LayerMask.NameToLayer("Bubble"))
        {
            Destroy(this.gameObject);
        }
    }
}
