﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class AttackEvent : MonoBehaviour
    {
        // ***************用來觸發武器動的COLLIDER開關

        public GameObject weapeon;
        public Collider weapCol; //武器碰撞
        public GameObject[] VFX;
        private void Awake()
        {
            weapeon = GameObject.Find("Weapon_point");  //先抓到主物件以外的武器物件
                                                        //print(weapeon.name);
            weapCol = weapeon.GetComponent<Collider>();  //先存入武器的碰撞器
        }
        void Start()
        {
                                   
            weapCol.enabled = !enabled;//在還沒執行攻擊動作前, 先關閉武器的碰撞器 **** 已經影響到OPEN_COL這個Event 運行   //武器碰撞先關閉
        }
        public void FixedUpdate()
        {
          
        }
        public void Update()
        {
            
            // AttackEvent_1();
        }
        
        public void Event()
        {
            Debug.Log("It is work");
        }
        public void Close_Col() 
        {
            weapCol.enabled = !enabled;
            //print(weapCol.enabled);
        }
        public void Open_Col() //攻擊動畫的Event判定碰撞器的開啟
        {
            weapCol.enabled = enabled;
            //print(weapCol.enabled);
            //print("Open_Col");
        }
    }

}
